//package org.example.spring_security_authentication.service;
//
//import org.example.spring_security_authentication.model.AppUser;
//import org.example.spring_security_authentication.model.AppUserDto;
//import org.example.spring_security_authentication.model.AppUserRequest;
//import org.example.spring_security_authentication.repository.AppUserRepository;
//import org.springframework.security.core.userdetails.UserDetails;
//import org.springframework.security.core.userdetails.UserDetailsService;
//import org.springframework.security.core.userdetails.UsernameNotFoundException;
//import org.springframework.stereotype.Service;
//
//@Service
//public class AppUserService implements UserDetailsService {
//    private final AppUserRepository appUserRepository;
//
//    public AppUserService(AppUserRepository appUserRepository) {
//        this.appUserRepository = appUserRepository;
//    }
//
//    @Override
//    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
//
//        return null;
//    }
//
//    public AppUserDto insertUser(AppUserRequest appUserRequest) {
//        AppUser appUser = appUserRepository.insertUser(appUserRequest);
//        AppUserDto appUserDto = new AppUserDto();
//        appUserDto.setId(appUser.getId());
//        appUserDto.setName(appUser.getName());
//        appUserDto.setEmail(appUser.getEmail());
//        appUserDto.setRole(appUser.getRole());
//
//        return appUserDto;
//    }
//
//}
