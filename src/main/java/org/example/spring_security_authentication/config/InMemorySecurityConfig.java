package org.example.spring_security_authentication.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.Customizer;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.security.web.SecurityFilterChain;

@Configuration
@EnableWebSecurity
public class InMemorySecurityConfig {
    private final PasswordEncoder passwordEncoder;

    public InMemorySecurityConfig(PasswordEncoder passwordEncoder) {
        this.passwordEncoder = passwordEncoder;
    }

    /*
     * Set value into memory it use for small project with less user
     * for some external source. So we will attempt to configure the application by hard coding a few users
     * and storing them in memory
     */

    @Bean
    UserDetailsService userDetailsService() {
        UserDetails user = User.builder()
                .username("User")
                .password(passwordEncoder.encode("123"))
                .roles("USER")
                .build();

        UserDetails admin = User.builder()
                .username("Admin")
                .password(passwordEncoder.encode("321"))
                .roles("ADMIN")
                .build();

            System.out.println("User Password :" + user.getPassword());
            System.out.println("Admin Password :" + admin.getPassword());

        return new InMemoryUserDetailsManager(user, admin);
    }


    @Bean
    public SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
        http
                .csrf().disable()
                .authorizeHttpRequests(request -> request
                        .requestMatchers("/welcome").authenticated()
                        .requestMatchers("/user").hasRole( "USER")
                        .requestMatchers("/admin").hasRole("ADMIN")
                        .requestMatchers("/bye").permitAll()
                        .anyRequest()
                        .authenticated()
                )
                .formLogin().defaultSuccessUrl("/welcome", true)
                .and()
                .httpBasic(Customizer.withDefaults());
        return http.build();
    }
}
