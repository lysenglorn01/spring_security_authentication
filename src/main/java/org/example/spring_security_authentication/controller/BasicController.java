package org.example.spring_security_authentication.controller;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
//@RequestMapping("/api/v1")
//@SecurityRequirement(name = "basicAuth")
public class BasicController {
    @GetMapping("/welcome")
    public String welcome(){
        return ("welcome to....");
    }
    @GetMapping("/user")
    public String user(){
        return ("User Testing");
    }

    @GetMapping("/admin")
    public String name(){
        return("This is admin page");
    }

    @GetMapping("/bye")
    public String bye(){
        return("Good bye....");
    }

}
