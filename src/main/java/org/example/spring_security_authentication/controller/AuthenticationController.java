//package org.example.spring_security_authentication.controller;
//
//import io.swagger.v3.oas.annotations.security.SecurityRequirement;
//import org.example.spring_security_authentication.model.AppUserDto;
//import org.example.spring_security_authentication.model.BaseApiResponse;
//import org.example.spring_security_authentication.model.AppUserRequest;
//import org.example.spring_security_authentication.service.AppUserService;
//import org.springframework.http.HttpStatus;
//import org.springframework.http.ResponseEntity;
//import org.springframework.web.bind.annotation.*;
//
//@RestController
//@RequestMapping("/auth")
//@SecurityRequirement(name = "basicAuth")
//public class AuthenticationController {
//    private final AppUserService appUserService;
//
//    public AuthenticationController(AppUserService userService) {
//        this.appUserService = userService;
//    }
//
//    @GetMapping("/test")
//    public String test(){
//        return "hello";
//    }
//
//    @PostMapping("/register")
//    public ResponseEntity<?> insertUser(@RequestBody AppUserRequest appUserRequest){
//        AppUserDto appUserDto  = appUserService.insertUser(appUserRequest);
//        BaseApiResponse<AppUserDto> response = BaseApiResponse.<AppUserDto>builder()
//                .message("success insert new user")
//                .status(HttpStatus.OK)
//                .payloat(appUserDto)
//                .build();
//        return ResponseEntity.ok(response);
//    }
//}
