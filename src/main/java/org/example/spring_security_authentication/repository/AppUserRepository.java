//package org.example.spring_security_authentication.repository;
//
//import org.apache.ibatis.annotations.Mapper;
//import org.apache.ibatis.annotations.Param;
//import org.apache.ibatis.annotations.Select;
//import org.example.spring_security_authentication.model.AppUser;
//import org.example.spring_security_authentication.model.AppUserRequest;
//
//@Mapper
//public interface AppUserRepository {
//    @Select("""
//            INSERT INTO security_role
//            (user_name, email, password, role)
//            VALUES (#{user.name}, #{user.email}, #{user.password}, #{user.role})
//            RETURNING *
//            """)
//    AppUser insertUser(@Param("user") AppUserRequest appUserRequest);
//}
