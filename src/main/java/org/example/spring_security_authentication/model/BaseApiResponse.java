//package org.example.spring_security_authentication.model;
//
//import lombok.AllArgsConstructor;
//import lombok.Builder;
//import lombok.Data;
//import lombok.NoArgsConstructor;
//import org.springframework.http.HttpStatus;
//
//@Data
//@AllArgsConstructor
//@NoArgsConstructor
//@Builder
//public class BaseApiResponse<T> {
//    private String message;
//    private HttpStatus status;
//    private T payloat;
//}
