create table security_role
(
    id        serial primary key,
    user_name varchar(50) unique not null,
    email     varchar(50) unique not null,
    password  varchar(250)       not null,
    role      varchar(50) unique not null
)